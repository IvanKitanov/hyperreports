﻿CREATE TABLE `departments` (
  `d_id` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `c_id` int(11) NOT NULL,
  PRIMARY KEY (`d_id`),
  KEY `city_id_idx` (`c_id`),
  CONSTRAINT `city_id` FOREIGN KEY (`c_id`) REFERENCES `cities` (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;