﻿CREATE TABLE `turnover` (
  `t_id` int(11) NOT NULL,
  `Amount` float NOT NULL,
  `Date` date NOT NULL,
  `e_id` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL,
  PRIMARY KEY (`t_id`),
  KEY `e_id_turnover_idx` (`e_id`),
  KEY `comp_id_turnover_idx` (`comp_id`),
  CONSTRAINT `comp_id_turnover` FOREIGN KEY (`comp_id`) REFERENCES `companies` (`comp_id`),
  CONSTRAINT `e_id_turnover` FOREIGN KEY (`e_id`) REFERENCES `employees` (`e_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;