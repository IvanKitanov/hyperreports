﻿CREATE TABLE `companies` (
  `comp_id` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `lastDocument` date NOT NULL,
  PRIMARY KEY (`comp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;