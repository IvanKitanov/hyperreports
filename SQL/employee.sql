CREATE TABLE `employees` (
  `e_id` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `d_id` int(11) DEFAULT NULL,
  `comp_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`e_id`),
  KEY `d_id_idx` (`d_id`),
  KEY `comp_id_idx` (`comp_id`),
  KEY `city_id_idx` (`city_id`),
  KEY `city_id3_idx` (`city_id`),
  CONSTRAINT `city_id3` FOREIGN KEY (`city_id`) REFERENCES `cities` (`city_id`),
  CONSTRAINT `comp_id` FOREIGN KEY (`comp_id`) REFERENCES `companies` (`comp_id`),
  CONSTRAINT `d_id` FOREIGN KEY (`d_id`) REFERENCES `departments` (`d_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;