package hyperreports.propertiesAccessor;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.util.Properties;

public class PropertiesAccessor {
    public String getDatabaseURL() throws IOException {
        try (InputStream input = new FileInputStream("F:\\JAVA\\HyperReports\\src\\hyperreports.main\\resources\\application.properties")) {
            PreparedStatement preparedStatement = null;
            Properties prop = new Properties();
            prop.load(input);
            String databaseURL = prop.getProperty("databaseURL");
            return databaseURL;
        }
    }
    public String getUsername() throws IOException {
        try (InputStream input = new FileInputStream("F:\\JAVA\\HyperReports\\src\\hyperreports.main\\resources\\application.properties")) {
            PreparedStatement preparedStatement = null;
            Properties prop = new Properties();
            prop.load(input);
            String username = prop.getProperty("username");
            return username;
        }

    }
    public String getPassword() throws IOException {
        try (InputStream input = new FileInputStream("F:\\JAVA\\HyperReports\\src\\hyperreports.main\\resources\\application.properties")) {
            PreparedStatement preparedStatement = null;
            Properties prop = new Properties();
            prop.load(input);
            String password = prop.getProperty("password");
            return password;
        }

    }
}
