package hyperreports.database;

import hyperreports.entities.City;
import hyperreports.propertiesAccessor.PropertiesAccessor;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CitiesRepository {
    private Connection connection;
    private static int cityId = 0;
    private static String cityName;
    private static PropertiesAccessor propertiesAccessor = new PropertiesAccessor();

    public CitiesRepository() {
        this.connection = DatabaseConnection.connectToDB();
    }

    public City selectCityByName(String name) {
        City city = new City();
        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM cities WHERE Name=?");) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                cityId = resultSet.getInt("city_id");
                cityName = resultSet.getString("name");
            }

            city.setName(cityName);
            city.setId(cityId);
            return city;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<City> selectAllFromCities() throws IOException, SQLException {
        List<City> cities = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM cities");) {

            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                City city = new City();
                city.setId(resultSet.getInt("city_id"));
                city.setName(resultSet.getString("name"));
                cities.add(city);
                return cities;
            }
            return cities;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return cities;
    }
    public void insertIntoCities(City city) throws SQLException, IOException {
        try(PreparedStatement  preparedStatement= connection.prepareStatement("INSERT INTO cities (Name) VALUES (?)");){
            preparedStatement.setString(1, city.getName());
            int resultSet = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateCitiesByCondition(City city, int id)  {
      try(PreparedStatement preparedStatement = connection.prepareStatement("UPDATE cities SET city_id = ?, Name = ? WHERE city_id=?");){
          preparedStatement.setInt(1, city.getId());
          preparedStatement.setString(2, city.getName());
          preparedStatement.setInt(3, id);
          int resultSet = preparedStatement.executeUpdate();
          System.out.println(resultSet + " rows affected.");

      }
      catch (SQLException ex) {
          ex.printStackTrace();
      }
    }

    public void deleteFromCitiesByCondition(int id) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM cities WHERE city_id=?");){
            preparedStatement.setInt(1, id);
            int resultSet = preparedStatement.executeUpdate();
            System.out.println(resultSet + " rows affected.");
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        connection.close();
    }
}
