package hyperreports.database;

import hyperreports.entities.Department;
import hyperreports.propertiesAccessor.PropertiesAccessor;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentsRepository {
    private static PropertiesAccessor propertiesAccessor= new PropertiesAccessor();
    private Connection connection;
    private static int  departmentId=0;
    private static String departmentName;
    private static int cityId=0;
    public DepartmentsRepository() {
        this.connection = DatabaseConnection.connectToDB();
    }

    public List<Department> selectAllFromDepartments()  {
        List<Department> departments= new ArrayList<>();
        try(PreparedStatement preparedStatement=connection.prepareStatement("SELECT * FROM departments")){
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Department department= new Department();
                department.setId(resultSet.getInt("d_id"));
                department.setName(resultSet.getString("Name"));
                department.setCityId(resultSet.getInt("c_id"));
                departments.add(department);
            }
            return  departments;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return departments;
    }
    public  Department selectDepartmentByName(String name) {
        Department department=  new Department();
       try(PreparedStatement preparedStatement=connection.prepareStatement("SELECT * FROM departments WHERE Name=?")){
           preparedStatement.setString(1,name);
           ResultSet resultSet = preparedStatement.executeQuery();
           while (resultSet.next()) {
               departmentId = resultSet.getInt("d_id");
               departmentName = resultSet.getString("name");
               cityId = resultSet.getInt("c_id");

           }
           department.setId(departmentId);
           department.setName(departmentName);
           department.setCityId(cityId);
           return department;

       } catch (SQLException e) {
           e.printStackTrace();
       }
        return null;

    }

    public  void insertIntoDepartments(Department department)  {
        try(PreparedStatement preparedStatement=connection.prepareStatement("INSERT INTO departments(Name, c_id) VALUES (?,?)")){
            preparedStatement.setString(1,department.getName());
            preparedStatement.setInt(2,department.getCityId());
            int resultSet = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public  void updateDepartmentssByCondition(Department department,int id)  {
        try (PreparedStatement preparedStatement=connection.prepareStatement("UPDATE departments SET d_id = ?, Name = ?, c_id=? WHERE d_id = ?")){
            preparedStatement.setInt(1,department.getId());
            preparedStatement.setString(2,department.getName());
            preparedStatement.setInt(3,department.getCityId());
            preparedStatement.setInt(4,id);
            int resultSet = preparedStatement.executeUpdate();
            System.out.println(resultSet+ " rows affected.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public  void deleteFromDepartmentsByCondition(int id){
        try (PreparedStatement preparedStatement=connection.prepareStatement("DELETE FROM departments WHERE d_id=?")){
            preparedStatement.setInt(1, id);
            int resultSet = preparedStatement.executeUpdate();
            System.out.println(resultSet+ " rows affected.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}