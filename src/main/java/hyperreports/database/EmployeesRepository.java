package hyperreports.database;

import hyperreports.entities.Employee;
import hyperreports.propertiesAccessor.PropertiesAccessor;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeesRepository {
    private Connection connection;

    private static int employeeID = 0;
    private static String employeeName ;
    private static Integer departmentId ;
    private static int cityId =0;
    private static int companyId = 0;

    public EmployeesRepository() {
        this.connection = DatabaseConnection.connectToDB();
    }

    public List<Employee> selectAllFromEmployees()  {
        List<Employee> employees= new ArrayList<>();
        try(PreparedStatement preparedStatement=connection.prepareStatement("SELECT * FROM employees"); ){
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Employee employee= new Employee();
                employee.setId(resultSet.getInt("e_id"));
                employee.setName(resultSet.getString("Name"));
                employee.setDepartmentId(resultSet.getInt("d_id"));
                employee.setCityId(resultSet.getInt("city_id"));
                employee.setCompanyId(resultSet.getInt("comp_id"));
                employees.add(employee);
            }
            return employees;}
        catch (SQLException e) {
            e.printStackTrace();
        }
        return  employees;
    }

    public  Employee selectEmployeesByName(String name) {
        Employee employee=new Employee();
       try(PreparedStatement preparedStatement=connection.prepareStatement("SELECT * FROM employees WHERE Name=?")){
           preparedStatement.setString(1,name);
           ResultSet resultSet = preparedStatement.executeQuery();
           while (resultSet.next()) {
               employeeID = resultSet.getInt("e_id");
               employeeName = resultSet.getString("Name");
               departmentId = (Integer) resultSet.getObject("d_id");
               cityId = resultSet.getInt("city_id");
               companyId = resultSet.getInt("comp_id");
           }
           employee.setId(employeeID);
           employee.setName(employeeName);
           employee.setDepartmentId(departmentId);
           employee.setCompanyId(companyId);
           employee.setCityId(cityId);
           return employee;
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return null;
    }

    public  void insertIntoEmployees(Employee employee)  {
        try(PreparedStatement preparedStatement=connection.prepareStatement("INSERT INTO employees (Name, d_id, comp_id,city_id) VALUES (?,?,?,?)")){
            preparedStatement.setString(1,employee.getName());
            preparedStatement.setObject(2,employee.getDepartmentId());
            preparedStatement.setInt(3,employee.getCompanyId());
            preparedStatement.setInt(4,employee.getCityId());
            int resultSet = preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public  void updateEmployeesByCondition(Employee employee, int id) {
        try(PreparedStatement preparedStatement=connection.prepareStatement("UPDATE employees SET e_id=?, Name=?, d_id=?, comp_id=?, city_id=? WHERE e_id = ?")){
            preparedStatement.setInt(1,employee.getId());
            preparedStatement.setString(2,employee.getName());
            preparedStatement.setObject(3,employee.getDepartmentId());
            preparedStatement.setInt(4,employee.getCompanyId());
            preparedStatement.setInt(5,employee.getCityId());
            preparedStatement.setInt(6,id);
            int resultSet = preparedStatement.executeUpdate();
            System.out.println(resultSet+ " rows affected.");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public  void deleteFromEmployeesByCondition(int id){
        try(PreparedStatement preparedStatement=connection.prepareStatement("DELETE FROM employees WHERE e_id=?")) {
            preparedStatement.setInt(1, id);
            int resultSet = preparedStatement.executeUpdate();
            System.out.println(resultSet + " rows affected.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
