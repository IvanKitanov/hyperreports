package hyperreports.database;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnection {
    public static Connection connectToDB() {

        Connection connection = null;
        try (InputStream input = new FileInputStream("F:\\JAVA\\HyperReports\\src\\main\\resources\\application.properties")) {

            Properties prop = new Properties();
            prop.load(input);
            String databaseURL = prop.getProperty("databaseURL");
            String username = prop.getProperty("username");
            String password = prop.getProperty("password");

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection(databaseURL, username, password);
        } catch (IOException | IllegalAccessException | InstantiationException | SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        return connection;
    }
}
