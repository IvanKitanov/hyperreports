package hyperreports.database;

import hyperreports.entities.Company;
import hyperreports.propertiesAccessor.PropertiesAccessor;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CompaniesRepository {
    private Connection connection;
    private static PropertiesAccessor propertiesAccessor= new PropertiesAccessor();
    private   static int companyId=0;
    private static String companyName;
    private  static Date lastDocument;

    public CompaniesRepository() {
        this.connection = DatabaseConnection.connectToDB();
    }

    public List<Company> selectAllFromCompanies(){
        List<Company> companies= new ArrayList<>();
        try(PreparedStatement preparedStatement=connection.prepareStatement("SELECT * FROM companies")){
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Company company= new Company();
                company.setId(resultSet.getInt("comp_id"));
                company.setName(resultSet.getString("name"));
                company.setLastDOcument(resultSet.getDate("lastDocument"));
                companies.add(company);

            }
            return companies;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return  companies;
    }

        public  Company selectCompanyByName(String name) throws IOException, SQLException {
            Company company = new Company();
            try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM companies WHERE Name=?")) {
                preparedStatement.setString(1, name);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    companyId = resultSet.getInt("comp_id");
                    companyName = resultSet.getString("name");
                    lastDocument = resultSet.getDate("lastDocument");

                }
                company.setId(companyId);
                company.setName(companyName);
                company.setLastDOcument(lastDocument);
                return company;

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }

    public  void insertIntoCompanies(Company company)  {
        try(PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO companies(Name, lastDocument) VALUES (?,?)")){
            preparedStatement.setString(1, company.getName());
            preparedStatement.setDate(2, company.getLastDocument());
            int resultSet = preparedStatement.executeUpdate();
        }

        catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public  void updateCompaniesByCondition(Company company, int id)  {
        try (PreparedStatement preparedStatement=connection.prepareStatement("UPDATE companies SET comp_id = ?, Name = ?, lastDocument=? WHERE comp_id = ?")) {
            preparedStatement.setInt(1,company.getId());
            preparedStatement.setString(2,company.getName());
            preparedStatement.setDate(3,company.getLastDocument());
            preparedStatement.setInt(4,id);
            int resultSet = preparedStatement.executeUpdate();
            System.out.println(resultSet+ " rows affected.");

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public  void deleteFromCompaniesByCondition(int id) {
        try(PreparedStatement preparedStatement=connection.prepareStatement("DELETE FROM companies WHERE comp_id=?")) {
            preparedStatement.setInt(1, id);
            int resultSet = preparedStatement.executeUpdate();
            System.out.println(resultSet+ " rows affected.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}




