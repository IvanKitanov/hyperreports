package hyperreports.database;

import hyperreports.entities.Turnover;
import hyperreports.propertiesAccessor.PropertiesAccessor;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TurnoverRepository {
    private Connection connection;
    private static PropertiesAccessor propertiesAccessor= new PropertiesAccessor();

    public TurnoverRepository() {
        this.connection = DatabaseConnection.connectToDB();
    }


    public List<Turnover> selectAllFromTurnover()  {
        List<Turnover> turnovers= new ArrayList<>();

        try(PreparedStatement preparedStatement=connection.prepareStatement("SELECT * FROM turnover")){
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Turnover turnover= new Turnover();
                turnover.setId(resultSet.getInt("t_id"));
                turnover.setAmount(resultSet.getDouble("Amount"));
                turnover.setDate(resultSet.getDate("Date"));
                turnover.setE_id(resultSet.getInt("e_id"));
                turnover.setComp_id(resultSet.getInt("comp_id"));
                turnovers.add(turnover);

            }
            return turnovers;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  turnovers;
    }
    public  void insertIntoTurnover(Turnover turnover) {

            try(PreparedStatement preparedStatement=connection.prepareStatement("INSERT INTO turnover (Amount, Date, e_id,comp_id) VALUES (?,?,?,?)")){
                preparedStatement.setDouble(1,turnover.getAmount());
                preparedStatement.setDate(2,turnover.getDate());
                preparedStatement.setInt(3,turnover.getE_id());
                preparedStatement.setInt(4,turnover.getComp_id());
                int resultSet = preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }



    public  void updateTurnover(Turnover turnover,int id)  {

        try(PreparedStatement preparedStatement=connection.prepareStatement("UPDATE turnover SET t_id=?, Amount=?, Date=?, e_id=?, comp_id=? WHERE t_id = ?")){
            preparedStatement.setInt(1,turnover.getId());
            preparedStatement.setDouble(2,turnover.getAmount());
            preparedStatement.setDate(3,turnover.getDate());
            preparedStatement.setInt(4,turnover.getE_id());
            preparedStatement.setInt(5,turnover.getComp_id());
            preparedStatement.setInt(6,id);
            int resultSet = preparedStatement.executeUpdate();
            System.out.println(resultSet+ " rows affected.");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public  void deleteFromTurnover(int id){

        try(PreparedStatement preparedStatement=connection.prepareStatement("DELETE FROM turnover WHERE t_id=?")){
            preparedStatement.setInt(1, id);
            int resultSet = preparedStatement.executeUpdate();
            System.out.println(resultSet+ " rows affected.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
