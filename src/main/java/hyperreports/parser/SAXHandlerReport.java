package hyperreports.parser;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import hyperreports.database.*;
import hyperreports.entities.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class SAXHandlerReport extends DefaultHandler {

        private List<Report> reports = null;
        private Report report = null;
        private String elementValue;
        private String currentCity;

        @Override
        public void startDocument() throws SAXException {
            reports = new ArrayList<Report>();

        }
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (qName.equalsIgnoreCase("city")) {

                if (attributes.getLength() > 0) {
                    currentCity=attributes.getValue("name");
                }
            }
            if (qName.equalsIgnoreCase("department")) {
                report = new Report();

                if(attributes.getLength() > 0) {

                    String departmentName = attributes.getValue("name");
                    report.setDepartmentName(departmentName);
                    report.setCityName(currentCity);

                }
            }

        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (qName.equalsIgnoreCase("department")) {
                reports.add(report);
            }

            if(qName.equalsIgnoreCase("employee")){
                report.setEmployeeName(elementValue);

            }
            if(qName.equalsIgnoreCase("turnover")){
                report.setTurnoverAmount(Double.valueOf(elementValue));

            }

        }
        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            elementValue = new String(ch, start, length);
        }
        public List<Report> getReports() {
            return reports;
        }
        public   List<Report> parseReportFromXML(String uri){
            List<Report> reports= new ArrayList<>();
            try{
                SAXParserFactory factory = SAXParserFactory.newInstance();
                SAXParser saxParser = factory.newSAXParser();
                SAXHandlerReport saxHandler = new SAXHandlerReport();
                saxParser.parse(uri,saxHandler);
                reports=saxHandler.getReports();
                return  reports;
            }
            catch(Exception ex){
                ex.printStackTrace();
            }

          return reports;
        }



    }
