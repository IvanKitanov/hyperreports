package hyperreports.parser;

import hyperreports.database.CitiesRepository;
import hyperreports.database.CompaniesRepository;
import hyperreports.database.EmployeesRepository;
import hyperreports.database.TurnoverRepository;
import hyperreports.entities.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class csvParser {


    public  static List<Report> ParseCSV(String fileName,String fulleName) throws SQLException, IOException{
        StringBuilder sb= new StringBuilder();
        StringBuilder sbCompName= new StringBuilder();
        StringBuilder sbDate= new StringBuilder();
        sb.append(fileName);
        sb.reverse();
        sb.delete(0,4);
        fileName=sb.toString();
        String[] arrOfStr = fileName.split("-", 2);
        sbCompName.append(arrOfStr[0]).reverse();
        sbDate.append(arrOfStr[1]).reverse();
        String companyName=sbCompName.toString();
        String date=sbDate.toString();
        Date lastDocument= Date.valueOf(date);

        List<Report> reports= new ArrayList<>();
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        try {


            br = new BufferedReader(new FileReader(fulleName));
            br.readLine();//skip the first line
            int i=0;

            while ((line = br.readLine()) != null) {
                Report report=new Report();
                String[] row = line.split(cvsSplitBy);
                row[0]=row[0].replace("\"", "");
                row[1]=row[1].replace("\"", "");
                row[2]=row[2].replace("\"", "");
                report.setCityName(row[0]);
                report.setEmployeeName(row[1]);
                report.setDepartmentName(null);
                report.setTurnoverAmount(Double.parseDouble(row[2]));
                reports.add(report);
                i++;
            }
            return reports;


        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {

            if (br != null) {
                try {
                    br.close();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
              }
   return reports;
    }
}

