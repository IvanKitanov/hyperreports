package hyperreports.Service;

import hyperreports.database.*;
import hyperreports.entities.*;
import hyperreports.parser.SAXHandlerReport;

import java.nio.file.Path;
import java.sql.Date;
import java.util.List;


import static hyperreports.parser.csvParser.ParseCSV;

public class DailyReportService {

    private static CompaniesRepository comaniesRepositories= new CompaniesRepository();
    private static CitiesRepository citiesRepository= new CitiesRepository();
    private static DepartmentsRepository departmentsRepository= new DepartmentsRepository();
    private static EmployeesRepository employeesRepository=new EmployeesRepository();
    private static TurnoverRepository turnoverRepository= new TurnoverRepository();
    private static  SAXHandlerReport saxHandlerReport= new SAXHandlerReport();


    public  void  importFile(Path path) throws Exception {
        String fileName=getFileNameAndExtension(path);
        StringBuilder stringBuilder= new StringBuilder();
        stringBuilder.append(fileName);
        stringBuilder.reverse();
        String reversedFileName= stringBuilder.toString();
        if(reversedFileName.substring(0,4).equalsIgnoreCase("vsc.")){


            List<Report> reports= ParseCSV(fileName,path.toString());
            StringBuilder sb= new StringBuilder();
            StringBuilder sbCompName= new StringBuilder();
            StringBuilder sbDate= new StringBuilder();
            sb.append(fileName);
            sb.reverse();
            sb.delete(0,4);
            fileName=sb.toString();
            String[] arrOfStr = fileName.split("-", 2);
            sbCompName.append(arrOfStr[0]).reverse();
            sbDate.append(arrOfStr[1]).reverse();
            String companyName=sbCompName.toString();
            String date=sbDate.toString();
            Date lastDocument= Date.valueOf(date);

            Company company= new Company(companyName,lastDocument);
            Company compareCompany = comaniesRepositories.selectCompanyByName(companyName);
            if(company.getName().equals(compareCompany.getName())){
                if(company.getLastDocument().compareTo(compareCompany.getLastDocument())==1){
                    company.setId(compareCompany.getId());
                    comaniesRepositories.updateCompaniesByCondition(company,company.getId());
                }
            }
            else {
                comaniesRepositories.insertIntoCompanies(company);
            }
            for(int i=0;i<reports.size();i++){
                City city=new City(reports.get(i).getCityName());
                City compareCity = citiesRepository.selectCityByName(reports.get(i).getCityName());
                if(!city.getName().equals(compareCity.getName())){
                    citiesRepository.insertIntoCities(new City(reports.get(i).getCityName()));
                }
                City cityIterator=citiesRepository.selectCityByName(reports.get(i).getCityName());
                Company companyIterator=comaniesRepositories.selectCompanyByName(companyName);
                Employee employee=new Employee(reports.get(i).getEmployeeName(),null,companyIterator.getId(),cityIterator.getId());
                Employee compareEmployee = employeesRepository.selectEmployeesByName(reports.get(i).getEmployeeName());
                if(!employee.getName().equals(compareEmployee.getName()) ){
                    employeesRepository.insertIntoEmployees(new Employee(reports.get(i).getEmployeeName(),null,companyIterator.getId(),cityIterator.getId()));
                }
                Employee employeeIterator=employeesRepository.selectEmployeesByName(reports.get(i).getEmployeeName());
                turnoverRepository.insertIntoTurnover(new Turnover(reports.get(i).getTurnoverAmount(),companyIterator.getId(),employeeIterator.getId(),lastDocument));


         }
        }
        else if(reversedFileName.substring(0,4).equalsIgnoreCase("lmx.")){
            List<Report> reports=saxHandlerReport.parseReportFromXML(path.toString());

            StringBuilder sb= new StringBuilder();
            StringBuilder sbCompName= new StringBuilder();
            StringBuilder sbDate= new StringBuilder();
            sb.append(fileName);
            sb.reverse();
            sb.delete(0,4);
            fileName=sb.toString();
            String[] arrOfStr = fileName.split("-", 2);
            sbCompName.append(arrOfStr[0]).reverse();
            sbDate.append(arrOfStr[1]).reverse();
            String companyName=sbCompName.toString();
            String date=sbDate.toString();
            Date lastDocument= Date.valueOf(date);

            Company company= new Company(companyName,lastDocument);
            Company compareCompany = comaniesRepositories.selectCompanyByName(companyName);
            if(company.getName().equals(compareCompany.getName())){
                if(company.getLastDocument().compareTo(compareCompany.getLastDocument())==1){
                    company.setId(compareCompany.getId());
                    comaniesRepositories.updateCompaniesByCondition(company,company.getId());
                }
            }
            else {
                comaniesRepositories.insertIntoCompanies(company);
            }
            for(int i=0;i<reports.size();i++){
                City city=new City(reports.get(i).getCityName());
                City compareCity = citiesRepository.selectCityByName(reports.get(i).getCityName());
                if(!city.getName().equals(compareCity.getName())){
                    citiesRepository.insertIntoCities(new City(reports.get(i).getCityName()));
                }
                City cityIterator=citiesRepository.selectCityByName(reports.get(i).getCityName());
                Company companyIterator=comaniesRepositories.selectCompanyByName(companyName);
                Department department=new Department(reports.get(i).getDepartmentName(),cityIterator.getId());
                Department compareDepartment = departmentsRepository.selectDepartmentByName(reports.get(i).getDepartmentName());
                if(!department.getName().equals(compareDepartment.getName())){
                    departmentsRepository.insertIntoDepartments(new Department(reports.get(i).getDepartmentName(),cityIterator.getId()));
                }
                Department departmentIterator=departmentsRepository.selectDepartmentByName(reports.get(i).getDepartmentName());
                Employee employee=new Employee(reports.get(i).getEmployeeName(),departmentIterator.getId(),companyIterator.getId(),cityIterator.getId());
                Employee compareEmployee = employeesRepository.selectEmployeesByName(reports.get(i).getEmployeeName());
                if(!employee.getName().equals(compareEmployee.getName()) ){
                    employeesRepository.insertIntoEmployees(new Employee(reports.get(i).getEmployeeName(),departmentIterator.getId(),companyIterator.getId(),cityIterator.getId()));
                }
                Employee employeeIterator=employeesRepository.selectEmployeesByName(reports.get(i).getEmployeeName());
                turnoverRepository.insertIntoTurnover(new Turnover(reports.get(i).getTurnoverAmount(),companyIterator.getId(),employeeIterator.getId(),lastDocument));
            // get list of objects from xml parser
        }
        }
        else System.out.println("Wrong file extention");
    }
    public  String getFileNameAndExtension(Path path){
        String fileName = path.getFileName().toString();
        return fileName;
    }




}
