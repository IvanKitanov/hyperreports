package hyperreports.entities;

public class Report {
    private String cityName;
    private String departmentName;
    private String employeeName;
    private double turnoverAmount;

    public Report(){

    }

    public Report(String cityName, String departmentName, String employeeName, double turnoverAmount ){
        this.cityName=cityName;
        this.departmentName=departmentName;
        this.employeeName=employeeName;
        this.turnoverAmount=turnoverAmount;

    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public double getTurnoverAmount() {
        return turnoverAmount;
    }

    public void setTurnoverAmount(double turnoverAmount) {
        this.turnoverAmount = turnoverAmount;
    }

    @Override
    public String toString() {
        return "Report{" +
                "cityName='" + cityName + '\'' +
                ", departmentName='" + departmentName + '\'' +
                ", employeeName='" + employeeName + '\'' +
                ", turnoverAmount=" + turnoverAmount +
                '}';
    }
}




