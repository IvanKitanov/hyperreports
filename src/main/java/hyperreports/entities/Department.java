package hyperreports.entities;

public class Department {

    private int id;
    private String name;
    private int cityId;

    public Department(){

    }
    public Department(String name, int cityId){
        super();
        this.name=name;
        this.cityId=cityId;
    }
    public Department(int id,String name, int cityId){
        super();
        this.id=id;
        this.name=name;
        this.cityId =cityId;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public int getCityId(){
        return cityId;
    }
    public void setCityId(int cityId){
        this.cityId =cityId;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", companyId=" + cityId +
                '}';
    }
}
