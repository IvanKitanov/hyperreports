package hyperreports.entities;

import java.sql.Date;

public class Company  {

    private  int id;
    private String name;
    private Date lastDocument;

    public Company(){

    }


    public  Company(String name, Date lastDocument){
        super();
        this.name=name;
        this.lastDocument=lastDocument;
    }
    public  Company(int id, String name, Date lastDocument){
        super();
        this.id=id;
        this.name=name;
        this.lastDocument=lastDocument;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getLastDocument() {
        return lastDocument;
    }

    public void setLastDOcument(Date lastDocument) {
        this.lastDocument = lastDocument;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastDocument=" + lastDocument +
                '}';
    }
}
