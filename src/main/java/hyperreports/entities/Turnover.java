package hyperreports.entities;
import java.sql.Date;


public class Turnover {
    private  int id;
    private double amount;
    private int comp_id;
    private int e_id;
    private Date date;

    public Turnover(){

    }
    public Turnover(double amount, int comp_id, int e_id, Date date){
        super();
        this.amount=amount;
        this.comp_id=comp_id;
        this.e_id=e_id;
        this.date=date;
    }
    public Turnover(int id,double amount, int comp_id, int e_id, Date date){
        super();
        this.id=id;
        this.amount=amount;
        this.comp_id=comp_id;
        this.e_id=e_id;
        this.date=date;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getComp_id() {
        return comp_id;
    }

    public void setComp_id(int comp_id) {
        this.comp_id = comp_id;
    }

    public int getE_id() {
        return e_id;
    }

    public void setE_id(int e_id) {
        this.e_id = e_id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Turnover{" +
                "id=" + id +
                ", amount=" + amount +
                ", comp_id=" + comp_id +
                ", e_id=" + e_id +
                ", date=" + date +
                '}';
    }
}
