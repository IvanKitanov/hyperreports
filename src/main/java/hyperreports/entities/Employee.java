package hyperreports.entities;

public class Employee {
    private int id;
    private String name;
    private Integer departmentId;
    private int companyId;
    private int cityId;

    public Employee(){
        this.departmentId=null;
    }


    public Employee(String name, Integer departmentId, int companyId, int cityId ){

        super();
        this.name=name;
        this.departmentId=departmentId;
        this.companyId=companyId;
        this.cityId=cityId;
    }

    public Employee(int id, String name, Integer departmentId, int companyId, int cityId ){

        super();
        this.id=id;
        this.name=name;
        this.departmentId=departmentId;
        this.companyId=companyId;
        this.cityId=cityId;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", departmentId='" + departmentId + '\'' +
                ", companyId=" + companyId +
                ", cityId=" + cityId +
                '}';
    }
}

